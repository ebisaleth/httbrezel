name := "httbrezel"

version := "1.0"

scalaVersion := "2.12.2"


// https://mvnrepository.com/artifact/io.spray/spray-json_2.11
libraryDependencies += "io.spray" % "spray-json_2.12" % "1.3.3"


// https://mvnrepository.com/artifact/com.typesafe.akka/akka-actor_2.11
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.4.19"

libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.0.9"

//http://nrinaudo.github.io/kantan.csv/
libraryDependencies += "com.nrinaudo" %% "kantan.csv" % "0.2.1"

//config file
libraryDependencies += "com.github.melrief" %% "pureconfig" % "0.6.0"