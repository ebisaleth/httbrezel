
import java.io.File

import spray.json._
import DefaultJsonProtocol._ // if you don't supply your own Protocol (see below)

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{FileIO, Source}

import scala.collection.immutable.Seq
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.parsing.json.JSON
import scala.util.{Failure, Success, Try}

case object AkkaBox {


  implicit val system = ActorSystem("System") //Hello

  private implicit val materializer = ActorMaterializer()

  def create_request(verb: HttpMethod, endpoint: String, heads: Map[String, String], payload: Option[JsValue], params: Map[String, String]): HttpRequest = {
    HttpRequest(
      method = verb,
      uri = endpoint + (if(params.nonEmpty)"?" else "") + params.map { case (k, v) => k + "=" + v }.mkString("&"),
      entity = payload match {
        case Some(json) => HttpEntity(ContentTypes.`application/json`, json.toString)
        case None => HttpEntity.Empty
      },
      headers = heads.toList.map { case (k, v) => new RawHeader(k, v) }
    )
  }


  def request_and_parse_json(r: HttpRequest) : Option[String] = {
    val f = Http().singleRequest(r)
      .map { res =>
        // very dangerous and also terrible
        Try {
          res.entity.asInstanceOf[HttpEntity.Strict].getData().decodeString("utf8")
        } match {
          case Success(m) => Some(m)
          case Failure(m) => None
        }

      }
    Await.result(f, 20 seconds)
  }

  def apply(verb: HttpMethod, endpoint: String, heads: Map[String, String], payload: Option[JsValue], params: Map[String, String]): Option[String] = {
    val r = create_request(verb, endpoint, heads, payload, params)
    println(r)
    request_and_parse_json(r)
  }

}
