import akka.http.scaladsl.model.HttpMethods
import spray.json._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.io.StdIn


object Main extends App {




  def single_file_payload(code: String, language: String, mainfilename: String, arg: String = "") = JsObject(
    "mainfilename" -> JsString(mainfilename),
    "files" -> JsArray(List(
      JsObject(
      "name" -> JsString(mainfilename),
      "content" -> JsString(code)
    ))),
    "arg" -> JsString(arg),
    "language" -> JsString(language)
  )

  def legacyPayload(code : String) : JsObject = JsObject(
    "id" -> JsString("bMC4wOTcxNTMzMTA5MjYyNzEyMg--"),
  "language" -> JsNumber(15),
  "code" -> JsString("[{\"name\":\"main.hs\",\"value\":\"" + code + "\",\"current\":true,\"base64\": false} ]"),
  "stdin" -> JsString(""),
  "filename" -> JsString("main.hs")
  )

  // code examples

  val finite_haskell = "main = print $ 11"

  val infinite_haskell = "main = print $ last [1..]"

  val long_haskell = "\nmain = print $ last $ primesTo 10000000\n\nprimesTo m = eratos [2..m] where\n   eratos (p : xs) \n      | p*p > m   = p : xs\n      | otherwise = p : eratos (xs `minus` [p*p, p*p+p..m])\n                                    -- map (p*) [p..]  \n                                    -- map (p*) (p:xs)   -- (Euler's sieve)\n \nminus a@(x:xs) b@(y:ys) = case compare x y of\n         LT -> x : minus  xs b\n         EQ ->     minus  xs ys\n         GT ->     minus  a  ys\nminus a        b        = a "

  val pda = "% pda-tool written by Marinus Enzinger and Niels Heller\n% https://gitlab.cip.ifi.lmu.de/beckern/pda-tool\n%\n% required arguments: -s <init_state> -w <word to parse>\n% optional arguments:\n% -b,--stack-symbol <arg>   the symbol for the initial bottom of the stack\n% -e,--empty-symbol <arg>   the symbol for the empty string\n%\n% -s,--start-state <arg>    the start state of the automaton\n% -w,--word <arg>           the input word to parse\n%\n% Transitions layout:\n% transitionName: fromState, readSymbol, topOfStack -> toState, newTopOfStack\n\n% Example testing for unequal number of a's and b's in input:\nd1: z, a, # -> z, A#\nd2: z, a, A -> z, AA\nd3: z, a, B -> z, µ\nd4: z, b, # -> z, B#\nd5: z, b, B -> z, BB\nd6: z, b, A -> z, µ\nd7: z, µ, A -> z', A\nd8: z, µ, B -> z', B\nd9: z', µ, A -> z', µ\nd10: z', µ, B -> z', µ\nd11: z', µ, # -> z', µ"

  val loop = "# --- LOOP-interpreter written by elordin: https://github.com/elordin/loop.\n\n# compute x0 := x1 * x2; \n# example arguments: 0 3 4 \n\nLOOP x1 DO\n    LOOP x2 DO\n        x0 := x0 + 1\n    END\nEND"

  val scala_code = "object HelloWorld {\n  def main(args: Array[String]): Unit = {\n    println(\"Hello, world!\")\n  }\n}"

  val goto = "M1 x0  := x1 + 0;\nM2 x9 := x2 + 0;\nM3 IF x9 = 0 THEN GOTO M5;\nM4   x9 := x9 - 1;\nM5   x0 := x0 + 1;\nM6 GOTO M3"

  val lodash = "const _ = require('lodash'); console.log(_)"


  def test_coco: Unit = {

    //val auth_token = "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1NGI2MTYwMC1hNjNkLTQyMGMtYWRjYS1hYjlhMmQxMWM3MTgiLCJleHAiOjE1MTM4Njc4MjYsIm5iZiI6MCwiaWF0IjoxNTEzNzgxNDI2LCJpc3MiOiJodHRwOi8vbWFuZ2FpYS5wbXMuaWZpLmxtdS5kZTozMTExL2F1dGgvcmVhbG1zL2Nyb3dkbGVhcm5pbmciLCJhdWQiOiJiYWNrc3RhZ2UiLCJzdWIiOiIzZWY0MGM4Yy1hNjcyLTQwMjEtYWYwMi0yNDdmYzlkNTg3ODYiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJiYWNrc3RhZ2UiLCJub25jZSI6ImY0M2UyYzg2LTg3NjgtNDVlZC04MDMxLWJkNzMzZWIzMTY0YyIsInNlc3Npb25fc3RhdGUiOiJmOTNiODBhZS1kYTNkLTQ0ZGEtOWVlYS1lMzgzYjc4ZDAxNDQiLCJjbGllbnRfc2Vzc2lvbiI6IjdmNTA3NzIwLTRjMmItNGIwMC1iY2I3LTM0N2E1MzU3YzE4ZCIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJzdHVkZW50Il19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsInZpZXctcHJvZmlsZSJdfX0sIm5hbWUiOiJTcWFyYWIgQmVldGxlIiwicHJlZmVycmVkX3VzZXJuYW1lIjoic3FhcmFiIiwiZ2l2ZW5fbmFtZSI6IlNxYXJhYiIsImZhbWlseV9uYW1lIjoiQmVldGxlIiwiZW1haWwiOiJzcWFyYWJAcG1zLmlmaS5sbXUuZGUifQ.HUUPz_Vf4i7xzKTpvQ_sni8VCTccXYUBTEVM52C9A1gzBowMSpjxtzrL-InOIHIF4vh4-710x5KmtHReWbrN4UkEp6len1w-c_JmoScsYMFnNSkwS49LwvwGt5_7MZ1cNAoTSi_NIYrUKfD_tVVevfBwtY-tE8bPQmfCue4EOpQisKoQ1ZWl0dDSSu34FMSd_Z319ue47UnY3gUuQbqe7Wt9Pm_jN8lHCJqV-m2cq-OE7OwNq8hML7PHHH7tSesYBsjUJthzMPUpQByX5e3nMACyAcnHXF99RSRGgr7qh9XBqIzdkUHsJDnGMRtLunudM3dS1LbQg5GMrh9QBxmB1A"
    //val auth_token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJTV3VPYkwweXZPcmdaQ1lMNVdwd1I0WHp4U0F2YXdkcTdhR21LRUlsV29FIn0.eyJqdGkiOiI4Mjk1MGQwOC00YjM1LTQxZjEtYWI0OC1hMTQzZmI4ODRmNmUiLCJleHAiOjE1MjkxMTQyMjAsIm5iZiI6MCwiaWF0IjoxNTI5MDcxMDIwLCJpc3MiOiJodHRwczovL2JhY2tzdGFnZTJhdXRoLnBtcy5pZmkubG11LmRlOjgwODAvYXV0aC9yZWFsbXMvY3Jvd2RsZWFybmluZyIsImF1ZCI6InByb2plY3RzIiwic3ViIjoiNzU0Y2U5OTUtMjQzMy00NzQwLThkMjEtY2NkYTJhZWFmNjY3IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoicHJvamVjdHMiLCJub25jZSI6ImJiZTY1YWUxLTMyMmQtNGE2MC04NTQ4LWUyMmRiMDdiMDliYyIsImF1dGhfdGltZSI6MTUyOTA1MjcxNywic2Vzc2lvbl9zdGF0ZSI6ImIxOWQ0ODBjLTdiYWEtNDk0NS04ODJiLTk4YWIzYTM3YjJjZiIsImFjciI6IjAiLCJjbGllbnRfc2Vzc2lvbiI6ImIwY2Y5NGQyLTY0ZjEtNDBkNi1hZTg2LWVhM2FlMzM4OTNjNyIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sIm5hbWUiOiJuaWVscyBoZWxsZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJuaWVscyIsImdpdmVuX25hbWUiOiJuaWVscyIsImZhbWlseV9uYW1lIjoiaGVsbGVyIiwiZW1haWwiOiJuaWVscy5oZWxsZXJAaWZpLmxtdS5kZSJ9.N3KFTUa1mq22CkyWR2UEfy2ZQh2sUKc6YPmsZzzf0MO1e6gt699x2P_zRFxxkC8fHAdbR7N50xLNMKevOhJX3OW2lnS2zpMjuN9zDv09WF5o30yT0wk1O9H6eLsyW6icdp3MCjkrXx_4BOp3iEKNjG9QnNx5McPu8qnyi5WfCatr3epvJvNj9a237DaejVIIbxKjywSqDrwMjfODv42ZfcPaCl_mMGu5B5rvM-woOkWgmdtK374KF6kL0EkqHkNRSzn0VEIpaqAk5jFjb4r7D3xWACDqFnox3GKxTHS8YZXf-BYlL6FL6Hth-8gbCmFA7yUVM6YNrpz6FxTvK4ilyg"
    val auth_token = "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiJhOGFkNGMyYy0xZWZkLTQyYzUtYTIwYy1iZWM1MTlkNTY5NDIiLCJleHAiOjE1MjkxNTc3MTgsIm5iZiI6MCwiaWF0IjoxNTI5MDcxMzE4LCJpc3MiOiJodHRwOi8vbWFuZ2FpYS5wbXMuaWZpLmxtdS5kZTozMTExL2F1dGgvcmVhbG1zL2Nyb3dkbGVhcm5pbmciLCJhdWQiOiJwcm9qZWN0cyIsInN1YiI6IjdmNTFlMmExLTE0OTAtNGYzZS1hYTUzLWZiMzhiM2RkNzcwMiIsInR5cCI6IkJlYXJlciIsImF6cCI6InByb2plY3RzIiwibm9uY2UiOiI0ZWNiYWNkNy00YjhiLTQxODEtYTRiMi1hYTE3ODRiODQ4NTYiLCJzZXNzaW9uX3N0YXRlIjoiMDJmOTZmYWItYzU3MS00YWFjLWI2MzAtN2E2YTU4MzkxNTQzIiwiY2xpZW50X3Nlc3Npb24iOiJkY2VhNTRlNS0xYTZiLTQ2M2YtYjU5NC1lMTQ4ZjgxZjJhNDQiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsic3R1ZGVudCJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJ2aWV3LXByb2ZpbGUiXX19LCJuYW1lIjoiQWxpY2UgQWFyb25zb24iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhbGljZSIsImdpdmVuX25hbWUiOiJBbGljZSIsImZhbWlseV9uYW1lIjoiQWFyb25zb24iLCJlbWFpbCI6ImFsaWNlQG1haWwuY29tIn0.j6AXkO5FOT6Rh8pE8Gm2736ao35LEodH_JeH0nNTms34iN4z0bgB3gp1Eai4AVTUfLX2TemdJl0UHPiIugK-mgQEnsE0MCMNoRsL2GTCPhXrqvHenRDl9gXYxVG3soKkXrPybY7YzWXxBrPIJCN0j6FcNKgnS3wRUeUfsS6UDqWEy9GAJfxWt9FxMp_unk4t1qpUGMHAF-lrEnZIXfQDeFplNBo4TVrLFFwJXz10K0nii77n9DQGsSqNLx_aL33FhEJXKcfbU2BKTEJshOLpriQV-KktfWolVUTMlKq7xa0rnw274BaebqIbJoRXTBLLgF5T0S2v1VIyE9hyhersYw"
    val auth_map = Map("Authorization" -> ("Bearer " + auth_token))


    val URL = "http://mauke:9000/compile"

    val posts = (0 to 31).toList.map(n => if (n%2
      == 0) AkkaBox(HttpMethods.POST, URL, auth_map, Some(legacyPayload(infinite_haskell)), Map.empty)
                                          else          AkkaBox(HttpMethods.POST, URL, auth_map, Some(legacyPayload(finite_haskell)), Map.empty))

    StdIn.readLine() // let it run until user presses return

    println(posts)

  }

  def test_coco2: Unit = {

    val auth_token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJTV3VPYkwweXZPcmdaQ1lMNVdwd1I0WHp4U0F2YXdkcTdhR21LRUlsV29FIn0.eyJqdGkiOiJjMWE0Y2NjMC0wNTY4LTQ5ZWItYjMwNy0wZDgyYWU0MjJmMzciLCJleHAiOjE1MjA2NTE2MDQsIm5iZiI6MCwiaWF0IjoxNTIwNjA4NDA0LCJpc3MiOiJodHRwczovL2JhY2tzdGFnZTJhdXRoLnBtcy5pZmkubG11LmRlOjgwODAvYXV0aC9yZWFsbXMvY3Jvd2RsZWFybmluZyIsImF1ZCI6InByb2plY3RzIiwic3ViIjoiMmE5OWFmMDMtOWFiZi00ZjgxLWE2MzEtNWM1NTc5NjQ2MjMxIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoicHJvamVjdHMiLCJub25jZSI6IjkzZjZkMGYwLTVkMjYtNDY1Zi05OGE0LTcyZDc2N2FmNzdhMiIsImF1dGhfdGltZSI6MTUyMDYwODM3OCwic2Vzc2lvbl9zdGF0ZSI6ImU5ZTMzMjk5LWE0OGItNDI2Ni05OTZjLWJmODE1YjI3OTI5OSIsImFjciI6IjAiLCJjbGllbnRfc2Vzc2lvbiI6IjgxMTE1NjEzLTYzYWMtNDM4My05MmM1LWFiNTk3ZTc1ZjJjMCIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sIm5hbWUiOiJFbGlzYWJldGggUHJvZnVuY3Rvcl9PcHRpY3MiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJlLWJldGgiLCJnaXZlbl9uYW1lIjoiRWxpc2FiZXRoIiwiZmFtaWx5X25hbWUiOiJQcm9mdW5jdG9yX09wdGljcyIsImVtYWlsIjoiZWxpc2FiZXRoQGJvYi5tYWlsLmNvbSJ9.X5eSP5E_q9ZhdQpQ7q7Z-xgkX68Z0_yWeJzpTLJnlUdjoZ52plYMKGM2Wpzr3YtMvntDQIm6cq6xwtvSFhFfROIf0r4Jdck6VV0hqM0e8b7ZcA65hpxu2gFDUNcnU0IEqRMtJS2SQ3fsSGt0cQi6ZYePEzRSOplMn-gWY25q2CIyM_vx7FZ-hB1GDo5xju5r4LZ2yBoyVpnbBw4oZ6FlJholUfoTFHXnIATiWM9saCQ2TJ_tW0J--LvSiYFOi_Ym5YWvedsEZUsDPx6fuozMueMN_SA1NggBq1FluTT_u7uyH-O0_0W6N74SBswvLp5I5uFe-6XP5XITv8kEp9C9iw"
    val auth_map = Map("Authorization" -> ("Bearer " + auth_token))

    val URL = "http://nukutipipi.pms.ifi.lmu.de:8008"


    val posts = (0 to 16).toList.map(_ => {
      val r = AkkaBox(HttpMethods.POST, URL, auth_map, Some(single_file_payload(goto, "goto", "goto", "")), Map.empty)
      Thread.sleep(250)
      r
    })

    println(posts)

    var done = false
    while(!done) {
      println("Press RETURN to try to retrieve your results.")

      scala.io.StdIn.readLine()

      val gets: List[Option[String]] = posts.collect { case Some(str) => str }
        .map(s => s.split(":").last.replace("\"", "").replace("}",""))
        .map(id => AkkaBox(HttpMethods.GET, URL, auth_map, None, Map("id" -> id)))

      if(gets.length==posts.length){
        println(gets);
        done = true;
      } else {
        println("not all results finished yet.")
      }
    }
  }


  test_coco2

  AkkaBox.system.terminate()

}
